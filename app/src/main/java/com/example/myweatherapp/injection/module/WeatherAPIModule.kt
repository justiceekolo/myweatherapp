package com.example.myweatherapp.injection.module

import com.example.myweatherapp.api.WeatherApi
import com.example.myweatherapp.api.WeatherInterceptor
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = arrayOf(GSONModule::class))
class WeatherAPIModule {

    @Provides @Singleton
    fun provideApi(gson:Gson): WeatherApi {
        val apiClient = OkHttpClient.Builder().addInterceptor(WeatherInterceptor()).build()

        return Retrofit.Builder().apply {
            baseUrl(WeatherApi.BASE_URl)
            addConverterFactory(GsonConverterFactory.create(gson))
            client(apiClient)
        }.build().create(WeatherApi::class.java)
    }

}