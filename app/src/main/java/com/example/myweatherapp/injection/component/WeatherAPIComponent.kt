package com.example.myweatherapp.injection.component

import com.example.myweatherapp.injection.module.WeatherAPIModule
import com.example.myweatherapp.presenter.MainPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(WeatherAPIModule::class))
interface WeatherAPIComponent {
    fun inject(presenter: MainPresenter)
}