package com.example.myweatherapp.presenter

import com.example.myweatherapp.BuildConfig
import com.example.myweatherapp.api.WeatherApi
import com.example.myweatherapp.dto.ForecastDetails
import com.example.myweatherapp.dto.ForecastResponse
import com.example.myweatherapp.helpers.ErrorTypes
import com.example.myweatherapp.ui.MainView
import com.example.myweatherapp.viewModel.ForecastItemViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MainPresenter(val view: MainView ) {
    @Inject lateinit var api: WeatherApi

    fun getSevenDaysForecast(cityName:String) {
        if (BuildConfig.OPEN_WEATHER_API_KEY == "<OPEN_WEATHER_API_KEY_HERE>") {
            view.showErrorToast(ErrorTypes.MISSING_API_KEY)
            return
        }
        view.showSpinner()
        api.forecast(cityName, 7).enqueue(object : Callback<ForecastResponse> {

            override fun onResponse(call: Call<ForecastResponse>, response: Response<ForecastResponse>) {
                response.body()?.let {
                    createListOfForecastForView(it)
                    view.hideSpinner()
                } ?: view.showErrorToast(ErrorTypes.NO_RESULT_FOUND)
            }

            override fun onFailure(call: Call<ForecastResponse>, t: Throwable) {
                view.showErrorToast(ErrorTypes.CALL_ERROR)
                t.printStackTrace()
            }
        })
    }

    private fun createListOfForecastForView(forecastResponse: ForecastResponse) {
        val forecasts = mutableListOf<ForecastItemViewModel>()

        for (details : ForecastDetails in forecastResponse.forecast) {
            val dayTemp = details.temperature.dayTemperature
            val forecastItem = ForecastItemViewModel(degreeDay = dayTemp.toString(),
                date = details.date, icon = details.description[0].icon, description = details.description[0].description)
            forecasts.add(forecastItem)
        }

        view.updateForecast(forecasts)
    }

}