package com.example.myweatherapp.api

import com.example.myweatherapp.dto.ForecastResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("forecast/daily/")
    fun forecast(@Query("q") cityName: String, @Query("cnt") dayCount: Int) : Call<ForecastResponse>

    companion object {
        val BASE_URl = "http://api.openweathermap.org/data/2.5/"
    }

}