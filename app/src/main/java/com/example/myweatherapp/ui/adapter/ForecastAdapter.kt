package com.example.myweatherapp.ui.adapter



import android.os.Build
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myweatherapp.R
import com.example.myweatherapp.viewModel.ForecastItemViewModel
import kotlinx.android.synthetic.main.forecast_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class ForecastAdapter() : RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {

    var forecastList = mutableListOf<ForecastItemViewModel>()

    fun addForecast(list : List<ForecastItemViewModel>){
        forecastList.clear()
        forecastList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.forecast_list_item, parent, false)
        return ForecastViewHolder(view, parent)
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        forecastList[position].let {
            holder.bind(forecastElement = it)
        }
    }

    override fun getItemCount(): Int {
        return forecastList.size
    }

    class ForecastViewHolder(itemView: View, viewGroup:ViewGroup) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(forecastElement : ForecastItemViewModel) {
            itemView.degreeText.text = "${forecastElement.degreeDay} °C"
            itemView.descriptionText.text = "${forecastElement.description}"
            itemView.dateText.text = getDate(forecastElement.date)
            Glide.with(itemView.context)
                .load("http://openweathermap.org/img/w/${forecastElement.icon}.png")
                .into(itemView.weatherIcon)
        }

        private fun  getDate(date: Long): String {
            val timeFormatter = SimpleDateFormat("dd.MM.yyyy")
            return timeFormatter.format(Date(date*1000L))
        }

        override fun onClick(v: View?) {
            Log.d("RecyclerView", "CLICK!")
            val inflater:LayoutInflater = LayoutInflater.from(itemView.context)
            val view = inflater.inflate(R.layout.popup_view,null)

            val popupWindow = PopupWindow(
                view,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                popupWindow.elevation = 10.0F
            }

            val dateText = view.findViewById<TextView>(R.id.date)
            val descriptionText = view.findViewById<TextView>(R.id.description)
            val temperatureText = view.findViewById<TextView>(R.id.temperature)
            val buttonPopup = view.findViewById<Button>(R.id.button_popup)
            temperatureText.text = itemView.degreeText.text
            dateText.text = itemView.dateText.text
            descriptionText.text = itemView.descriptionText.text

            buttonPopup.setOnClickListener{
                popupWindow.dismiss()
            }

            popupWindow.showAtLocation(
                itemView,
                Gravity.CENTER,
                0,
                0
            )


        }

    }

}