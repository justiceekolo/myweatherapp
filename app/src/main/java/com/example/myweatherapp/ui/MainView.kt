package com.example.myweatherapp.ui

import com.example.myweatherapp.helpers.ErrorTypes
import com.example.myweatherapp.viewModel.ForecastItemViewModel

interface MainView {
    fun showSpinner()
    fun hideSpinner()
    fun updateForecast(forecasts: List<ForecastItemViewModel>)
    fun showErrorToast(errorType: ErrorTypes)
}