package com.example.myweatherapp.viewModel

import android.os.Parcel
import android.os.Parcelable
import androidx.versionedparcelable.VersionedParcelize

@VersionedParcelize
data class ForecastItemViewModel (val degreeDay : String,
                                  val icon : String = "01d",
                                  val date : Long = System.currentTimeMillis(),
                                  val description: String = "No description") :  Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(degreeDay)
        parcel.writeString(icon)
        parcel.writeLong(date)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ForecastItemViewModel> {
        override fun createFromParcel(parcel: Parcel): ForecastItemViewModel {
            return ForecastItemViewModel(parcel)
        }

        override fun newArray(size: Int): Array<ForecastItemViewModel?> {
            return arrayOfNulls(size)
        }
    }
}