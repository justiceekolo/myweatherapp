package com.example.myweatherapp.dto

import com.google.gson.annotations.SerializedName

data class ForecastResponse (@SerializedName("city") var city : City,
                             @SerializedName("list") var forecast : List<ForecastDetails>)